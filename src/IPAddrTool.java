
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * @author huangqiang
 * @version TODO
 * @date 2021/5/23 10:18
 * @description TODO
 * @modified
 */
public class IPAddrTool {

    /**
     * @author  huangqiang
     * @description 随机产生邮件地址
     * @time    2021/5/23 10:41
     * @param   amount
     * @return  java.util.List<java.lang.String>
     */
    private List<String> getRandomIPs(int amount){

        List<String> ips=new ArrayList<>(amount);
        Random random=new Random();
        for (int k=0;k<amount;++k){
            String ip=String.join(".", List.of(random.nextInt(255)+1,random.nextInt(256),random.nextInt(256),random.nextInt(256)).stream().map((x)->x+"").toList());
            ips.add(ip);
        }
        return  ips;
    }


    // 第一种实现方式: 使用正则表达式
    public long isValidIpAddressV1(List<String> ips) {
        long start=System.currentTimeMillis();
        for (String ipAddress : ips) {
            if (ipAddress.isBlank()) {
                System.out.println(ipAddress);
                return -1L;
            }
            String regex =
                    "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\."
                            + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
                            + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
                            + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";
            if (!ipAddress.matches(regex)){
                System.out.println(ipAddress);
                return -1L;
            }
        }
        return  System.currentTimeMillis()-start;
    }

    // 第二种实现方式: 使用现成的工具类
    public long  isValidIpAddressV2(List<String> ips) {
        long start=System.currentTimeMillis();
        for (String ipAddress : ips) {
            if (ipAddress.isBlank()) {
                System.out.println(ipAddress);
                return -1L;
            }
            String[] ipUnits = ipAddress.split("\\.");
            if (ipUnits.length != 4) {
                System.out.println(ipAddress);
                return -1L;
            }
            for (int i = 0; i < 4; ++i) {
                int ipUnitIntValue;
                try {
                    ipUnitIntValue = Integer.parseInt(ipUnits[i]);
                } catch (NumberFormatException e) {
                    System.out.println(ipAddress);
                    return -1L;
                }
                if (ipUnitIntValue < 0 || ipUnitIntValue > 255) {
                    System.out.println(ipAddress);
                    return -1L;
                }
                if (i == 0 && ipUnitIntValue == 0) {
                    System.out.println(ipAddress);
                    return -1L;
                }
            }
        }
        return  System.currentTimeMillis()-start;
    }

    // 第三种实现方式: 不使用任何工具类
    public long isValidIpAddressV3(List<String> ips) {
        long start=System.currentTimeMillis();
        for (String ipAddress : ips) {
            char[] ipChars = ipAddress.toCharArray();
            int ipUnitIntValue = -1;
            boolean isFirstUnit = true;
            int unitsCount = 0;
            for (char c : ipChars) {
                if (c == '.') {
                    if (ipUnitIntValue < 0 || ipUnitIntValue > 255) {
                        System.out.println(ipAddress);
                        return -1L;
                    }
                    if (isFirstUnit && ipUnitIntValue == 0) {
                        System.out.println(ipAddress);
                        return -1L;
                    }
                    if (isFirstUnit) {
                        isFirstUnit = false;
                    }
                    ipUnitIntValue = -1;
                    unitsCount++;
                    continue;
                }
                if (c < '0' || c > '9') {
                    System.out.println(ipAddress);
                    return -1L;
                }
                if (ipUnitIntValue == -1) {
                    ipUnitIntValue = 0;
                }
                ipUnitIntValue = ipUnitIntValue * 10 + (c - '0');
            }
            if (ipUnitIntValue < 0 || ipUnitIntValue > 255) {
                System.out.println(ipAddress);
                return -1L;
            }
            if( unitsCount != 3){
                System.out.println(ipAddress);
                return -1L;
            }
        }
        return  System.currentTimeMillis() -start;
    }
    public static void main(String[] args) {
        IPAddrTool IPAddrTool =new IPAddrTool();
        System.out.println("======10000=======");
        List<String> ips= IPAddrTool.getRandomIPs(10000);
        System.out.println(IPAddrTool.isValidIpAddressV1(ips));
        System.out.println(IPAddrTool.isValidIpAddressV2(ips));
        System.out.println(IPAddrTool.isValidIpAddressV3(ips));
        System.out.println("======100000=======");
        ips= IPAddrTool.getRandomIPs(100000);
        System.out.println(IPAddrTool.isValidIpAddressV1(ips));
        System.out.println(IPAddrTool.isValidIpAddressV2(ips));
        System.out.println(IPAddrTool.isValidIpAddressV3(ips));
        System.out.println("======1000000=======");
        ips= IPAddrTool.getRandomIPs(1000000);
        System.out.println(IPAddrTool.isValidIpAddressV1(ips));
        System.out.println(IPAddrTool.isValidIpAddressV2(ips));
        System.out.println(IPAddrTool.isValidIpAddressV3(ips));
    }
}